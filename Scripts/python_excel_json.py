#!/usr/bin/python3

import os
import os.path
import pandas as pd

folder_path = input('enter file path (without the ending \"/\"): ')
file_name = input('enter file name: ')
json_file = input('enter new json file name: ')

full_path = folder_path + os.sep + file_name

if os.path.isfile(full_path) and full_path.endswith('.xlsx'):
    read_file = pd.read_excel(full_path)
    read_file.to_json(folder_path + os.sep + json_file)
else:
    print("Something went wrong")
