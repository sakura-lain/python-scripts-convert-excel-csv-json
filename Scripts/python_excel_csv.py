#!/usr/bin/python3

import os
import os.path
import pandas as pd

folder_path = input('enter file path (without the ending \"/\"): ')
file_name = input('enter file name: ')
csv_file = input('enter new csv file name: ')

full_path = folder_path + os.sep + file_name

if os.path.isfile(full_path) and full_path.endswith('.xlsx'):
    read_file = pd.read_excel(full_path)
    read_file.to_csv(folder_path + os.sep + csv_file, index = None, header = True)
else:
    print("Something went wrong")
