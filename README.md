# Python scripts to convert Excel files to CSV or JSON

Python scripts to convert Excel files to CSV or JSON.

See also [my scripts to convert ODs to CSV](https://gitlab.com/sakura-lain/ods-to-csv-conversion-scripts).